# PATH {{{

export PATH=/usr/local/bin:/usr/local/texlive/2017/bin/x86_64-darwin/:/Users/vittorio/.local/bin:$PATH

#path for mysql
#export PATH=${PATH}:/usr/local/mysql/bin

# }}}
# XDG {{{

# export CONFIG=${HOME}/git/dotfiles
# export CACHE=${HOME}/.cache

# }}}
# BASIC {{{

#elimino vittorio@MacBook-Pro-di-Vittorio quando sono loggato come vittorio
DEFAULT_USER=vittorio

# }}}
# AUTOCOMPLETION {{{

#see hidden files in autocompletion
setopt globdots

zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:approximate:*' max-errors 2 numeric

# }}}
# PLUGINS {{{

# modify ZGEN default plugin path
ZGEN_DIR="${HOME}/.zgen/plugins"

# load zgen
source "${HOME}/.zgen/zgen.zsh"

# oh-my-zsh
zgen oh-my-zsh
zgen oh-my-zsh plugins/git

# prezto
# zgen prezto
# zgen prezto environment
# zgen prezto terminal
# zgen prezto editor
# zgen prezto history
# zgen prezto directory
# zgen prezto spectrum
# zgen prezto utility
# zgen prezto completion
# zgen prezto prompt

# other
zgen load zsh-users/zsh-autosuggestions
zgen load mafredri/zsh-async
zgen load DFurnes/purer
zgen load zsh-users/zsh-syntax-highlighting
zgen load b4b4r07/enhancd init.sh

# purer
PURE_PROMPT_SYMBOL_COLOR=red
PURE_PROMPT_SYMBOL=">>>"

# }}}
# COMMAND VARIABLES {{{

## HISTORY
bindkey '\eOA' history-beginning-search-backward
bindkey '\e[A' history-beginning-search-backward
bindkey '\eOB' history-beginning-search-forward
bindkey '\e[B' history-beginning-search-forward

# }}}
# ALIASES {{{
# ZSHRC MANAGEMENT {{{
alias zshconfig="vim ${HOME}/.zshrc"
alias zshsource="source ${HOME}/.zshrc"
# }}}
# BETTER CORE COMMANDS {{{
alias opf='open .'
alias freespace='df -kH /Users/vittorio'
alias cl='clear'
alias cdh='cd ~ && clear'
alias vim='/usr/local/bin/vim'

## LS
alias lsa='exa -aHl'
alias tree='exa -T'

## C++
alias c11='c++ -std=c++11 --stdlib=libc++'
# }}}
# MUTT {{{
#alias mutt="neomutt -F $XDG_CONFIG_HOME/mutt/muttrc"
# }}}
# HLEDGER {{{
LEDGER_PATH="$HOME/git/ledger/"
LEDGER_FILE="$LEDGER_PATH/current.ledger"
alias hl='hledger -f $LEDGER_FILE'
alias hlr='hledger -f $LEDGER_FILE register'
alias hlb='hledger -f $LEDGER_FILE balance'
alias hla='hledger -f $LEDGER_FILE balance assets'
alias hlei='hledger -f $LEDGER_FILE balance income expenses not:mom not:income:regali -M -A -T'
alias hlpa='hledger -f $LEDGER_FILE balance pending anticipated not:mom not:income:regali -M -A -T'
alias hlmom='hledger -f $LEDGER_FILE balance mom --depth 2'
alias hltm='hledger -f $LEDGER_FILE balance income expenses not:mom not:travel not:income:regali  --depth 2 -p thismonth'
alias hlvim='vim $LEDGER_FILE'
alias hlsum='hlei && hlpa'
# }}}
# }}}
# FUNCTIONS {{{

######## WATCH MD FILE AND COMPILE TO LATEX
watch_md(){
    ls "$1" | entr sh -c "pandoc --pdf-engine=xelatex --toc --filter pandoc-citeproc $1 -o $1.pdf"
}

####### MANJARO - SYSTEMD
rc(){
	systemctl list-unit-files --type=service |\
	sed 's/.service//g' |\
	sed '/static/d' |\
	sed '/indirect/d' |\
	sed '/systemd/d' |\
	sed '/dbus-org/d' |\
	sed '/canberra/d'|\
	sed '/wpa_supplicant/d' |\
	sed '/netctl/d' |\
	sed '/rfkill/d' |\
	sed '/krb5/d' |\
	tail -n+2 |\
	head -n -2 |\
	sed 's/\(^.*enabled.*$\)/[x] \1/' |\
	sed 's/enabled//g' |\
	sed 's/\(^.*disabled.*$\)/[ ] \1/' |\
	sed 's/disabled//g' |\
	sed 's/[ \t]*$//' |\
	while read line; do
			if [[ $line == *'[x]'* ]]; then
				printf "\033[0;32m$line\n"
			else
				printf "\033[1;30m$line\n"
			fi
	done
}
#######

######## JUPYTER NOTEBOOKS
#jupyter-nbconvert --to=pdf --template=latex_nocode.tplx Note.ipynb
########

######## HLEDGER
add() {
  echo "Syncing..."
  git -C $LEDGER_PATH pull
  hledger add -f $LEDGER_FILE
  echo "Syncing..."
  git -C $LEDGER_PATH add $LEDGER_FILE
  git -C $LEDGER_PATH commit -m "update"
  git -C $LEDGER_PATH push origin master
  clear
}

# iadd() {
#   echo "Syncing..."
#   git -C ~/Documents/git/ledger pull
#   hledger iadd -f ~/Documents/git/ledger/ledger.journal
#   echo "Syncing..."
#   git -C ~/Documents/git/ledger add ledger.journal
#   git -C ~/Documents/git/ledger commit -m "update"
#   git -C ~/Documents/git/ledger push origin master
#   clear
# }
########

######## MYSQL/APACHE
#start apache mysql
startsrv() {
    sudo apachectl start
    echo "apachestl... Done."
    sudo /usr/local/mysql/support-files/mysql.server start
}

#stop apache mysql
stopsrv() {
    sudo apachectl stop
    echo "apachectl... Done."
    sudo /usr/local/mysql/support-files/mysql.server stop
}
########

######## BETTER CORE COMMANDS
#mkdir + cd
mkcd () {
  mkdir "$1"
  cd "$1"
}

#cd + lsa
cdl () {
    cd "$1"
    lsa
}

#git add commit push origin master
gitom () {
	git add .
	git commit -m "$1"
	git push origin master
}
########

######## YADM
dot () {
    yadm add $1
    yadm commit -m "$2"
    yadm push origin master
}
########

######## OTHERS
colortest () {
  T='•••'   # The text for the color test

    echo -e "\n         def     40m     41m     42m     43m     44m     45m     46m     47m";

    for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' \
               '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' \
               '  36m' '1;36m' '  37m' '1;37m';

      do FG=${FGs// /}
      echo -en " $FGs \033[$FG  $T  "

      for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
        do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
      done
      echo;
    done
    echo
} 
########
# }}}

# vim:foldmethod=marker:foldlevel=0
