" ENVIRONMENT {{{

filetype off                  " required
set nocompatible              " be iMproved, required
set modelines=1                 "look for modelines at the exact last line of the file
set laststatus=2

"}}}
" SPACES & TABS {{{

set backspace=indent,eol,start
set tabstop=4           " 4 space tab
set expandtab           " use spaces for tabs
set softtabstop=4       " 4 space tab

" }}}
" INDENTATION {{{

set shiftwidth=4		" 4 space block indentation
filetype indent on		" load type indent file from runtimepath
filetype plugin on 		" load type indent file from runtimepath
set autoindent
set pastetoggle=<F2>            "paste mode

" }}}
" UI {{{

set showcmd             " show command in bottom bar as you type
set wildmenu			" show menu for command autocomplete
set lazyredraw			" don't redraw too much the screen
set fillchars+=vert:┃	" change vertical splitting character
set number              " show real current line number
" set relativenumber      " show relative numbers for lines above and below

" }}}
" CURSOR {{{

set scrolloff=100        " leave rows above and below cursor line
set cursorline          " highlight current line

" }}}
" LINES {{{

set wrap               " soft-wrap lines
set linebreak          " soft-wrap lines only at certain characters (see :help breakat)
set showbreak=------>\  " line up soft-wrap prefix with the line numbers
set cpoptions+=n        " start soft-wrap lines (and any prefix) in the line-number area

" visual cursor up and down
vmap <silent> <Right> l
vmap <silent> <Left> h
vmap <silent> <Up> gk
vmap <silent> <Down> gj
nmap <silent> <Right> l
nmap <silent> <Left> h
nmap <silent> <Up> gk
nmap <silent> <Down> gj
imap <silent> <Up> <C-o>gk
imap <silent> <Down> <C-o>gj

" }}}
" AUTOCOMPLETION {{{

set showmatch           " higlight matching parenthesis

" }}}
" SEARCHING {{{

set ignorecase          " ignore case when searching
set incsearch           " search as characters are entered
set hlsearch            " highlight all matches

" }}}
 " FOLDING {{{

set foldmethod=syntax   " fold based on indent level
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
set foldlevelstart=10   " start with fold level of 1
nnoremap <space> za

" }}}
" LEADER {{{

let mapleader=","

" quickly modify vimrc and zshrc, and reload vimrc
nnoremap <leader>ev :vsp ~/.vimrc<CR>
nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source ~/.vimrc<CR>

" clear search highlight
nnoremap <leader><space> :noh<CR>

" save session to be restore with vim -S
" nnoremap <leader>s :mksession<CR>

" }}}
" VUNDLE {{{

set rtp+=${HOME}/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-commentary'
Plugin 'scrooloose/nerdtree'
Plugin 'lervag/vimtex'
" Plugin 'chriskempson/base16-vim'
Plugin 'itchyny/lightline.vim'
" Plugin 'mike-hearn/base16-vim-lightline'
Plugin 'junegunn/vim-easy-align'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'JuliaEditorSupport/julia-vim'
Plugin 'ledger/vim-ledger'
Plugin 'junegunn/goyo.vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'amix/vim-zenroom2'
Plugin 'vimwiki/vimwiki'
Plugin 'suan/vim-instant-markdown'

Plugin 'arcticicestudio/nord-vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" }}}
" COLORS {{{

syntax enable           " enable syntax processing
let base16colorspace=256  " Access colors present in 256 colorspace

set termguicolors
let g:nord_comment_brightness = 20
let g:nord_cursor_line_number_background = 1

colorscheme nord

" }}}
" PLUG CONFIG {{{

" Nerdtree
" leader + n to toggle
let NERDTreeShowHidden=1    "show dotfiles 
" when opening a directory, open nerdtree
map <leader>n :NERDTreeToggle<CR>    

" vimtex
let g:vimtex_compiler_latexmk = {'callback' : 0}
let g:vimtex_latexmk_build_dir='build'


"lightline
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

" Easy align interactive
vnoremap <silent> <Enter> :EasyAlign<cr>
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" au FileType markdown vmap <Leader>| :EasyAlign*<Bar><Enter>


"CtrlP
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_map = '<leader>sa'
let g:ctrlp_working_path_mode = 'c'

" ledger
let g:ledger_bin = 'hledger'

" vimwiki
let g:vimwiki_list = [{'path':'~/cloud/Dropbox/vimwiki',
  \ 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_global_ext = 0

" vim-instant-markdown - Instant Markdown previews from Vim
" https://github.com/suan/vim-instant-markdown
let g:instant_markdown_autostart = 0	" disable autostart
map <leader>md :InstantMarkdownPreview<CR>

" }}}

" vim:foldmethod=marker:foldlevel=0
